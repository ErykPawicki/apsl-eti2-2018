<html>
    <head>
        <title><?php echo $title ?></title>
    </head>
    <body>
        <h1>Hello world</h1>
        <?php include 'parts/flash-messages.php' ?>
        <?php if ($session->has('user')): ?>
            <p>Witaj, <?php echo $session->get('user') ?></p>

            <p><a href="<?php echo $router->generate('logout') ?>">Wyloguj</a></p>
        <?php else: ?>
            <form action="<?php echo $router->generate('login_check') ?>" method="post">
                <input name="username" value="" placeholder="Username" />
                <input name="password" type="password" value="" placeholder="Password" />

                <button type="submit">Zaloguj</button>
            </form>
        <?php endif ?>

        <a href="<?php echo $router->generate('homepage') ?>">Homepage</a>
        <a href="<?php echo $router->generate('article', ['id' => 1]) ?>">Article1</a>

        <?php echo $content; ?>
    </body>
</html>