<?php

namespace App\Session;

class Session
{
    private $sessionStarted = false;

    private $flashMessages;

    public function __construct()
    {
        $this->flashMessages = new FlashMessage($this);
    }

    public function start()
    {
        $this->sessionStarted = true;
        session_start();
    }

    /**
     * @param string $name
     * @param $value
     */
    public function set(string $name, $value, $global = false)
    {
        if ($global) {
            $globals = json_decode(($_SESSION['globals'] ?? '[]'), true);
            $globals[$name] = null;
            $_SESSION['globals'] = json_encode($globals);
        }

        if (!$this->sessionStarted) {
            $this->start();
        }

        $_SESSION[$name] = $value;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function has(string $name)
    {
        if (!$this->sessionStarted) {
            $this->start();
        }

        return isset($_SESSION[$name]);
    }

    /**
     * @param string $name
     * @param null $default
     * @return mixed|null
     */
    public function get(string $name, $default = null)
    {
        if (!$this->sessionStarted) {
            $this->start();
        }

        return $_SESSION[$name] ?? $default;
    }

    /**
     * @param string $name
     */
    public function remove(string $name)
    {
        unset($_SESSION[$name]);
    }

    public function close()
    {
        if (!$this->sessionStarted) {
            return;
        }

        session_write_close();
    }

    public function destroy()
    {
        if (!$this->sessionStarted) {
            $this->start();
        }
        session_destroy();
    }

    public function regenerate() {
        if (!$this->sessionStarted) {
            $this->start();
        }

        $passVariables = [];
        $globals = json_decode($_SESSION['globals'] ?? '[]', true);
        foreach (array_keys($globals) as $key) {
            $passVariables[$key] = $this->get($key);
        }

        $this->destroy();
        session_id(session_create_id());
        $this->start();

        foreach ($passVariables as $key => $value) {
            $this->set($key, $value, true);
        }
        $_SESSION['globals'] = json_encode($globals);
    }

    /**
     * @return array|mixed
     */
    public function getFlashMessages()
    {
        return $this->flashMessages->getMessages();
    }

    /**
     * @param string $type
     * @param string $message
     */
    public function setFlashMessage(string $type, string $message)
    {
        $this->flashMessages->setMessage($type, $message);
    }

    /**
     * @return bool
     */
    public function hasFlashMessages()
    {
        return !empty($this->flashMessages->peekMessages());
    }
}