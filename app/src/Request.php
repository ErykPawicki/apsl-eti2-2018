<?php

namespace App;

class Request
{
    private $pathParametes = [];

    private $post = [];

    private $method = 'GET';

    public function __construct(
        string $path,
        string $method,
        array $queryParameters,
        array $post
    ) {
        $this->path = $path;
        $this->queryParameters = $queryParameters;
        $this->post = $post;
        $this->method = $method;
    }

    public static function fromGlobals()
    {
        $uri = $_SERVER['REQUEST_URI'];
        $index = strpos($uri, '?');
        if ($index === false) {
            $path = $uri;
        } else {
            $path = substr($uri, 0, $index);
        }

        return new self($path, $_SERVER['REQUEST_METHOD'], $_GET, $_POST);
    }

    public function getPath()
    {
        return $this->path;
    }

    public function hasQueryParam(string $name)
    {
        return isset($this->queryParameters[$name]);
    }

    public function getQueryParam(string $name, $default = null)
    {
        return $this->queryParameters[$name] ?? $default;
    }

    public function getPathParameters()
    {
        return $this->pathParametes;
    }

    /**
     * @param array $params
     */
    public function setParameters($params)
    {
        $this->pathParametes = $params;
    }

    public function getParameter($name, $default = null)
    {
        return $this->pathParametes[$name] ?? $default;
    }

    public function getPost(string $name, $default = null)
    {
        return $this->post[$name] ?? $default;
    }

    public function isPost()
    {
        return strtoupper($this->method) === 'POST';
    }
}