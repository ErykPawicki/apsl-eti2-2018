<?php


namespace App\Controllers;

use App\Request;
use App\Response\RedirectResponse;
use App\Response\Response;
use App\Router;
use App\Session\Session;

class LoginCheckController implements ControllerInterface
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var Router
     */
    private $router;

    /**
     * LoginCheckController constructor.
     * @param Session $session
     * @param Router $router
     */
    public function __construct(Session $session, Router $router)
    {
        $this->session = $session;
        $this->router = $router;
    }

    public function __invoke(Request $request): Response
    {
        $response = new RedirectResponse(
            $this->router->generate('homepage')
        );

        if (!$request->isPost()) {
            return $response;
        }

        if (
            $request->getPost('username') === 'test' &&
            $request->getPost('password') === 'test123'
        ) {
            $this->session->regenerate();
            $this->session->set('user', 'Tester');
            $this->session->setFlashMessage('success', 'Zostałeś zalogowany do systemu.');
        } else {
            $this->session->setFlashMessage('error', 'Niepoprawne dane.');
        }

        return $response;
    }
}