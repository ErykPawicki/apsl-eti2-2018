<?php

namespace App\Controllers;

use App\Request;
use App\Response\JsonResponse;
use App\Response\Response;

class HomepageController implements ControllerInterface
{
    public function __invoke(Request $request): Response
    {
        return new JsonResponse([
            'HOMEPAGE TEST',
            'param1' => 123
        ]);
    }
}