<?php

namespace App\Controllers;

use App\Layout;
use App\Request;
use App\Response\LayoutResponse;
use App\Response\Response;
use App\Router;

class PageController implements ControllerInterface
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $layout;

    /**
     * @var Router
     */
    private $router;

    /**
     * PageController constructor.
     * @param Router $router
     * @param string $name
     * @param string $layout
     */
    public function __construct(Router $router, string $name, string $layout = 'default')
    {
        $this->name = $name;
        $this->layout = $layout;
        $this->router = $router;
    }

    public function __invoke(Request $request): Response
    {
        return new LayoutResponse($this->name, [
            'request' => $request,
            'router' => $this->router
        ], $this->layout);
    }
}